---
layout: job_page
title: "Director Federal Sales"
---

The Director of Federal Sales is responsible for building, managing and leading the Federal sales teams by overseeing sales, operations and strategies for new and existing customers of GitLab.

The right candidate will play a critical role in the creation and execution of a Federal Sales Program (Team, Strategy, Systems & Process) to maximize the adoption of GitLab within the Federal market.

## Responsibilities

* Drives, manages and executes the business and revenue of the federal sales team; develops, coaches and trains sales staff
* Analyzes regional market dynamics in an effort to maximize existing successes and to create new sales growth opportunities
* Educates team on significant industry factors including competitive products, regulations, trends, customer needs, and pricing
* Prepares forecasts, territory/industry management, and growth plans
* Establishes and reports on metrics to measure team performance; correct deficiencies where necessary
* Supports quotation and proposal efforts to partners, prospects and customers
* Ensures that the federal sales plan is aligned with and supports the corporate revenue goal
* Recruits, hires and trains staff members; fosters a successful and positive team environment

## Requirements for applicant

* At least five 5 years of sales management, selling software or hardware through a two tiered channel eco system including distribution, corporate resellers, and value added resellers (VARS) into Federal government
* At least 10 years of experience selling software or hardware through a two tiered channel eco system including distribution, corporate resellers, and value added resellers (VARS) into Federal government
* Must be a high performing sales manager with a proven track record of consistently exceeding established measurements for goals and objectives into Fed
* Must be a skilled motivator with demonstrated ability to coach and develop sales staff to exceed targeted sales objectives and deliver high quality service to customers
* Must have a proven ability to manage both direct and indirect employees in a matrix organization
* Preferred candidates will have experience in business-to-business selling and managing using a consultative sales approach, and will have strong demonstrated partnering and closing skills
* Must have expertise around GSA, Federal procurement and the various buying vehicles to enable growth in software sales via our distribution partners
* Excellent management and communication skills (written and verbal)
* Demonstrated knowledge of different sales methodologies and customer relationship management (CRM) systems: SFDC knowledge is desired
* Knowledge of open source enterprise software and/or version control is highly desirable
* Experience with partner recruitment, enablement and on going support
* Exceptional knowledge of Federal infrastructure and agencies
* Have history of working with and driving revenue within NSF, DOE Labs, DoD and Intel
* A security clearance is a plus

Avoid the [confidence gap](http://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
