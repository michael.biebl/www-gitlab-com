---
layout: markdown_page
title: "People Operations Standard Operating Procedures"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Role of People Operations Standard Operating Procedures

For more information about People Operations Policies please see [People Operations](https://about.gitlab.com/handbook/people-operations/). This page is dedicated toward how People Ops does what it does best.


### Setting up new contracts

New team hire contracts are found on the [Contracts](https://about.gitlab.com/handbook/contracts/) page, including instructions on how to set up new contracts.

#### Using HelloSign

When we need [contracts to be signed](https://about.gitlab.com/handbook/#signing-legal-documents) we use [HelloSign](https://hellosign.com).
Follow these steps to send out a signature request.

1. Choose who needs to sign. (just me, me & others, or just others)
1. Upload your document that needs signing
1. Enter the names of the people that need to sign
1. With more than one signature required, assign signing order (for contracts or offer letters, always have the GitLab Signatory sign first)
1. Add the People Ops team to the cc. Also add the hiring manager to the cc, if that person is different from the signatory.
1. Click on "Prepare Docs for signing"
1. Drag & Drop the Signature and Date fields to the corresponding empty spaces in the document (you can select the signee with the pop up screen)
1. For GitLab Inc. agreements, add a required text box to Exhibit A, and a supplemental larger text box below for a comment that is not required.
1. Add a title and a message for the recipient. For contract use for example: "Dear [...], You can sign [document type] with HelloSign. Once you've signed you will receive a copy by email. If you have any questions, feel free to reach out"
1. Request Signature.

Once you've sent out the document you will receive email notifications of the progress and a copy of the signed document after all parties have signed.
If you need to make a change, there is an "Edit & Resend" option in HelloSign. If a new document has to be uploaded, cancel the original and stage a new one to send for signatures.

#### Letter of Adjustment

When a team member receives any change in compensation we need to create a [Letter of Adjustment](https://about.gitlab.com/handbook/contracts/#letter-of-adjustment) instead of staging an entirely new contract. This document is signed by the Sr. Director of People Operations and the team member through HelloSign. Once the document has been signed, it is uploaded into BambooHR under the Contracts and Changes folder on the Documents Tab. Also, file any other supporting documentation, for example, an email with the approval to change the compensation. Information in BambooHR, TriNet, and HRSavvy should also be updated, if applicable.

### Processing changes

Any changes to a team member’s status, classification, promotion/demotion, pay increase/decrease and
bonus needs to be communicated via email to People Ops by the team member's manager or CEO. People Ops will then enter
the changes in BambooHR under the relevant section on the Jobs tab in the member’s profile; and then file the email in the Contracts and Changes folder in the Document tab.

Further changes may need to be [processed in TriNet](#changes-trinet) or Savvy to fully process the change. People Ops
is responsible for seeing the change through to completion. Once completed, People Ops
sends an email to the person reporting / requesting the change (member's manager or CEO)
to confirm this.

### Using BambooHR

We use [BambooHR](https://gitlab.bamboohr.com) to keep all team member information
in one place. All team members (all contract types) are in BambooHR.
We don't have one contact person but can call BambooHR if we want any changes made in the platform. The contact info lives in the Secretarial Vault in 1Password.

Some changes or additions we make to BambooHR require action from our team members.
Before calling the whole team to action:

1. document what the process will be regarding this action (e.g. "need information X") for
_new_ team members. (Recall that we strive to change processes by documenting them, not vice versa).
1. make sure _that it is necessary_  for the entire team to act, or whether the
work can be done by People Ops (even if this is tedious to do for People Ops, it
is preferred, so as not to burden the team),
1. make sure that the call to action is OK-ed by management,
1. and check the steps to be taken by testing them with a single team member or a
test account that does not have admin privileges.
1. in the note to the team, point to the documentation created in step 1, explain the need and
the ask, and who to turn to in case of questions.

### Adding a New Team Member to BambooHR

As part of [onboarding](https://about.gitlab.com/handbook/general-onboarding/), the People Ops Specialist will process new hires in BambooHR. Aside from the steps listed in the onboarding issue, this is a description of how to add the proper information into BambooHR.

Personal Tab

1. Verify the team member was given an Employee ID number.
1. Enter the appropriate Country.
1. Region: Should either be Americas, EMEA, or JAPAC.
1. Enter the team member's time zone.
1. Verify the work email is entered.

Jobs Tab

1. Hire Date - This will automatically populate with the day that the profile was transferred from Workable. Make sure to enter in the correct date.
1. Role
   * Leader - if director or above
   * Manager - if has any direct reports
   * Individual Contributor - all others
1. FLSA Code - This will either be exempt or non-exempt depending on how the role is classified. If there are questions on the classification, please ask the People Ops Specialist.
1. Reports to EID - Direct Manager's employee ID number.
1. Cost Center - Leave blank for now. This will become relevant as we scale.
1. Exec Name - Executive in the team member's reporting chain who reports to the CEO.
1. Payroll Type
   * Employee - paid through Payroll
   * Contractor - IND - Independent Contractor agreement
   * Contractor - C2C - Contractor Company agreement
1. Exception to IP Agreement - If they answered Yes, on the IP agreement in the contract.
1. Compensation Table
   * Effective Date - Hire Date
   * Pay Rate - Entered as if it were a payroll amount. For example, a US employee would be entered as their yearly amount divided by 24 payrolls in a year. A contractor would have their monthly contract amount listed.
   * Pay Per - Leave blank unless for an hourly employee
   * Pay Type - Use either Salary, Hourly, or Contract.
   * Pay Period - Select the pay period. Currently we have twice a month for the US, and monthly for all others.
   * Change Reason - New Hire
   * Comment - Please add any comments that are relevant from the contract terms.
1. Pay Frequency (Note: Pay Frequency times pay rate should equal annual compensation)
   * 12 for contractors, GitLab LTD, and LYRA
   * 12.96 for GitLab B.V. employees in the Netherlands
   * 13.92 for GitLab B.V. employees in Belgium
   * 24 for GitLab Inc. employees
1. On Target Earnings
   * If the new team member is on a commission or quarterly bonus plan according to their contract, please add the details into this table.
1. Currency Conversion
   * Every January and July the People Ops specialist will conduct a currency conversion for all team members.
   * Using either January 1 or July 1 as the currency conversion effective date, use Oanda for the currency conversion factor.
   * Enter the Local Annual Salary, and the converted salary in USD.
1. Job information
   * Effective Date - Hire Date
   * Location - Which entity the new team member is contracted through.
   * Division - Enter the appropriate division from the dropdown.
   * Department - Leave blank for now. This will become relevant as we scale.
1. Employment Status
   * Enter the hire date and set the status to active. Also leave a comment if there is anything of note in the contract.    

### Settings in BambooHR

Changing a Format (Example: Date)

1. Click on Settings
1. Choose Account
1. Select General Settings
1. Change the date format to match desired output

Adding a New Job Title

1. Click on Settings
1. Select Employee Field
1. Select Job Title
1. Add new job title to the drop down list

Add a New Division

1. Click on Settings
1. Select Employee Field
1. Select Division
1. Add new division

### Process Referral Bonus

If applicable, People Ops will process a [referral bonus](/handbook/#referral-bonuses).

#### Document a future bonus in BambooHR

1. Go to the employee who referred the new team member in BambooHR
1. Under the Jobs Tab click Update Bonus
1. Add the date the bonus will be paid if all conditions are met (90 days from hire of new employee)
1. Enter the bonus amount of $1,000
1. Enter the bonus type to be a Referral Bonus
1. Enter a note stating that this is a future bonus (this will be changed once the bonus has been paid)

#### Add a note to the new team member in BambooHR

We want to add a note to the new employee of who referred them, so that if they receive a discretionary bonus within the first six months, the referral also receives a discretionary bonus.

1. Go to the notes page under the new employee
1. Add a note of who referred this team member

#### Notification to Process a Bonus

BambooHR will send an email to PeopleOps on the date that the referral bonus should be paid for [processing](/handbook/people-operations/#using-trinet) through the applicable payroll system. If the team member is a contractor, send them an email (and cc Finance) to invoice the bonus.
Once the bonus has been processed, change the note in BambooHR to denote the referral bonus has been paid.

### Asset Tracking

Items paid for by the company are property of the company.

Assets with purchasing value in excess of $1000 USD are tracked in BambooHR, for assets of lower value we rely on the honor system. These assets come to People Ops attention by one of two ways: 1. People Ops makes the purchase on behalf of the team member, or 2. the Finance team notices the line items on expense reports / invoices and passes this along to People Ops.

The information is then entered into BambooHR (to track who has which piece of equipment) by People Ops, and it is included in the Fixed Asset Schedule by Finance (to track asset value and depreciation).

1. Go to the team member in BambooHR
1. Click on the Assets Tab
1. Click Update Assets
1. Enter Asset Category, Asset Description, Serial Number, Asset Cost, and Date Loaned
1. This process is repeated for each asset purchased

### Birthday Swag

BambooHR should send an email to People Ops the day before and the day of a team member's birthday. Celebrate by sending the team member an email (template below) and post on the #general channel on Slack so all team members can help them celebrate.

Birthday Email Template: "Happy Birthday! We've got a great [swag store](https://gitlab.mybrightsites.com/) set up. Please login or create an account with the swag store to receive your free birthday shirt! The username will be your GitLab email. Once you are are in the store you should see your options to order a GitLab birthday shirt. In order to receive free shipping please use the code: [Enter free shipping code]. Please let people ops know if you have any questions."

### Managing the PeopleOps onboarding and offboarding tasks

Here are some useful guidelines to set up everything for our new team members with the onboarding issue.
If Google Sheets or Docs are mentioned, these will be shared with you on Google Drive.

#### Onboarding

- **Select a Buddy**
In the "Buddy Spreadsheet" google sheet, select a buddy for the new team member that is in the same timezone, but a different functional group. Try to pair a technical team member with a non-technical team member and vice versa. Also, add the new team member to the sheet so they can also be a buddy one day!
- **Google Account**
Log on to the Google Admin console. Verify that the email that you have chosen for the new team member is not conflicting with a current account or team member's name. Select Users. Click the + sign to add a new user. Once you have created the handle, send an email to the new team member's personal email. Lastly, add the new team member to the New User's group under the Groups tab.
- **Add team member to availability calendar**
Make a new "all-day" event on the day the new team member starts "[Name] joining
as [Job title]". Make sure to select the calendar as GitLab Availability and not your own.
- **Give team member access to the GitLab availability calendar**
Go to your calendar window, under my calendars move your cursor over the calendar
and click the dropdown triangle. Select calendar settings and go to "Share this
calendar" in the top of the window. Enter the GitLab email address and scroll
down to set the permission setting to "make changes to events" Then save in the
lower left corner.
- **Add team member to the GitLab Birthdays calendar**
Go to your calendar window, under my calendar select the GitLab Birthdays calendar. Go to the team member's birthdate and add an all day event with their name as the title. Change the setting to repeat annually. Also add an entry for the team member's work anniversary. Change the color of the event to blue, make it an annual event, and include Name and Work Anniversary in the title. Share this calendar with the new team member.  
- **Add blank entry to team page**<a name="blank-entry"></a>
Login to [Gitlab.com](www.gitlab.com) and go to the www-gitlab-com project. In
the left menu click "Files," select "data," and choose the file called team.yml. In the top right
corner you can click "edit" to make edits to the code. Scroll all the way down
and copy-paste the code of the previous blank entry to the team page. Edit the
fields to the right info of the new hire and find the right job description URL on
the [Jobs](https://about.gitlab.com/jobs/) page. Do not include the employee's name in the commit message, only initials.  
 **Note** _This can be tricky, so if you run into trouble reach out to some of
your awesome colleagues in the #questions (channel) on Slack_
- **Add to phishing test platform**
Login to [Knowbe4](https://training.knowbe4.com/). Click on import users. Add the team member's email and import. You do not need to assign them to any groups. Go to the users page and update their email to also reflect their first and last name.
- **Add entry to Team Call agenda**
Open the Team Agenda google doc, and on the starting day add an Agenda item:
"[Hiring manager name]: Welcome [name new team member] joining as [job title]"
as the first item on the agenda. If the new team member is not starting on the next agenda day, add their name to the list of Team Members to be added, and keep an eye out for their start date to add them to the agenda.
- **Create new profile for team member in BambooHR, and input relevant data**
Once the new team member has been transferred from Workable to BambooHR, go to the Dashboard and search their name. Under the Personal tab, add country, work email, and personal email. Under the Job tab, enter the compensation from the contract. Note: We do not use the pay period or pay type option in this list, but BambooHR cannot remove it.
Next enter job information. Be sure to verify the proper title and contract type.
- **Invite to team meeting and GitLab 101 meeting**
Go to the team call meeting on the starting date of the team member and the next
scheduled GitLab 101 in the Availability calendar. Click on "edit event" to open.
On the right enter the team member's GitLab email address in the  "add guests"
section and click save. When asked select "all events" to add to all scheduled
meetings and "send" out the invitation.
- **Send swag codes to new team members**
To celebrate a new team member joining our team, send an email with $50 swag credits for our [Swag Store](https://gitlab.mybrightsites.com/), as noted in the onboarding issue. You can get the codes for the swag by finding the Google doc titled "Tshirt and stickers coupons 2.12.16".
- **Ordering Equipment**
PeopleOps will order required equipment. New employee should send links to items they would like to have and PeopleOps will order it and have it shipped to their address. To order notebooks, PeopleOps has a contact at Apple, so if a new employee is considering buying and expensing a macbook, first reach out to PeopleOps for the Apple contact info.
- **Order business cards**
Go BambooHR and enter the info needed for the Business Cards. This is found by clicking on More, then selecting Business Cards. Select Update Business Card Requests and fill out all the pertinent fields.
Log in with your specific credentials to [MOO](https://www.moo.com/m4b/account/login). Select Create New Packs. Create a blank pack, then go to GitLab packs and select the newly created default. Edit the Title to be the team member's name. Click on the preview to edit the name, job title, email, phone number, twitter handle, and location. Once you have made sure all of the information is correct on the business card you can select how many cards you would like to order (standard is 50) and add the cards to the cart. When you are checking out, update which shipping center you would like the cards sent from at the top right by selecting the proper flag. Enter in the team member's shipping address. Place the order with the applicable shipping and billing addresses.
- **Add team member to Expensify (only with employees)**<a name="add-expensify"></a>
Login to [Expensify](https://www.expensify.com/signin) and go to "Admin" in the top menu. Select the right policy based upon the entity that employs the new team member. Select "People" in the left menu. Select "Invite" and add the GitLab email. Edit the message to be sent for the employee. Click "invite". If the team member should be added as an admin to be able to also add new team members, update them to a [domain admin](https://docs.expensify.com/advanced-admin-controls/domain-members-and-groups).
- **Add team member to Beamy**<a name="add-beamy"></a>
Login in to access the settings for the [Beam](https://suitabletech.com/accounts/login/). In the top menu move your cursor over the blue login button. Go to "Manage your beams". Click on "manage" in the lower left corner. Enter the GitLab email and scroll down to find the newly addedd email. Check the box for "Auto connect".
- **Add team member into the Summit info sheets**
Add the team member's name to the proper Google sheet to make sure they enter flight details and will be assigned a room.
- **Add team member to our Egencia platform**<a name="add-egencia"></a>
Log into Egencia and go to the menu option "manage users". Choose "new user account" and fill in the passport name of the new team member.
As username choose the same handle as on our dev domain. Enter the GitLab email address and uncheck the newsletter box.
Lastly assign the department in which the new team member is working.
- **Upgrade team member's Zoom account to Pro**<a name="make-zoom-pro"></a>
Log in to [Zoom](https://gitlab.zoom.us) with the generic admin credentials stored in 1Password. Find the user in the "User Management" tab, and click "edit", then select "Pro" (the default is Basic). If the user does not yet appear in Zoom, add them through the "Add Users" button at the top of the User Management tab. A Pro account allows the individual to have calls longer than 30 minutes. If we've run out of Pro licenses, send an email to our account manager at Zoom, as listed in a shared note on 1Password.

### Using TriNet

#### Entering New Hires into TriNet

Employer enters the employee data in the HR Passport with the information below

1. Under the My Staff tab- select new hire/rehire and a drop down menu will appear.
1. Enter all of the necessary information:
    * Company name auto-populates
    * SS
    * Form of address for hire (Mr. Ms, etc.)
    * First name
    * Last name
    * Middle name or initial
    * Country
    * Address
    * Home phone
    * Home email
    * Gender
    * Ethnicity (you must select something - guess if employee declines to state)
    * Military status

At the bottom of the screen, select next

    * TriNet’s start date
    * Reason - drop down menu with options
    * Employment type - Full time or PT options
    * Select reg/temp bubble
    * Employee Class - drop down between regular and commission
    * Estimated annual wages (does not include anything besides base salary)
    * Benefit class
    * Future benefits class -
    * Standard Hours/week - Part time or Full time
    * Business Title - see org chart
    * Job Code - no need to enter anything here
    * FLSA status- drop down options are exempt, non-exempt, computer prof-non-exempt, computer prof- exempt
    * Supervisor - drop down menu of names
    * Compensation Basis
    * Compensation Rate
    * Departments
    * Work Location - drop down menu
    * Pay Group - only one option
    * Employee ID - not necessary
    * Work email
    * Grouping A/level - not necessary
    * Grouping B/sponsor- not necessary

Select next or save (if you select save, it will hold your information)

    * Vacation/PTO - drop down menu only provides one option- select this
    * Sick- drop down menu only provides one option- select this
    * Personal Time - leave blank
    * Floating Holidays - leave blank
    * Birthdate - mm/dd/yyyy
    * Workers compensation- select unknown and it will default to our principle class code for our industry
Window: Describe employees job duties - simple description

After submission -  you will receive a prompt for final submission, select and submit.

Note: if you save the information to finish at a later date, go to the Work Inbox and select New Hires Not Submitted to continue.

1. The employee receives a welcome email the night before their start date.
1. The employee is prompted to log on, complete tax withholding (W4 data), direct deposit information, section 1 of the I-9, and benefits election (if eligible).
1. The employer logs in to HR Passport and is prompted by way of work inbox item, to complete section 2 of the I-9.

#### Adding a Trusted Advisor to TriNet

A trusted advisor is a team member in People Ops who is granted access to TriNet, but is not an employee of GitLab Inc. Since they are not added to the system as a part of onboarding, we need to add them as a trusted advisor to make any changes within the system.

1. Add to the TriNet Platform
    1. From TriNet Passport homepage, click on the tab My Company
    1. Select My Workplace, Trusted Advisor Setup
    1. Complete the information in the online form
    1. Click Submit
1. Assign the Role of Trusted Advisor
    1. From TriNet Passport homepage, click Find
    1. Select Find Person by Name
    1. Type in the person’s name
    1. Click search
    1. Once the name appears, click on it
    1. From the options to the left, select Assign Roles
    1. Click on Add
    1. From the drop down select the access role desired
    1. Click Add

You can also follow the same process for assigning a role to a GitLab Inc. employee who is already in the TriNet system.


#### Making changes in TriNet
{: #changes-trinet}

##### Add a New Location

1. Go to HR Passport homepage
1. Click Find
1. Click Find Location.
1. When search field appears, leave blank and click Search.
1. Click on Add location.
1. Complete location information. For a remote location, enter the location (ex. WA remote) in all fields except city, state and zip.
1. Click Add.

##### Transfer Employee to Different Location

1. Go to HR Passport homepage.
1. Click Find.
1. Select find person by Name.
1. Type the name, click search.
1. From the choices, select the name.
1. On the left side of the screen, select Employment Data.
1. Select Employee Transfer.
1. Change location and fill in necessary information.
1. Select Update.

##### Record Pay Change

1. Go to HR Passport homepage.
1. Click Find.
1. Select find person by Name.
1. Type the name, click search.
1. Select Employment Data on the left side of the screen.
1. Click Employee Pay Change
1. Select Effective Date.
1. Enter new rates.
1. Answer the worker's compensation question.
1. Save.

##### Record Job Change

1. Go to HR Passport homepage.
1. Click Find.
1. Select find person by Name.
1. Type the name, click search.
1. Select Employment Data on the left side of the screen.
1. Select the reason for the change.
1. Enter the effective date.
1. Click next.
1. Select the reason.
1. Change the business title. Also, change any other applicable fields.
1. Enter yes or no under the worker's comp section.
1. Click Update

##### Enter a Bonus into TriNet

1. Go to HR Passport homepage
1. Under "My Company" select "Payroll Entry & Admin"
1. Select the proper payroll
1. Under Other Earnings, select the BNO code and enter the amount of the bonus.
1. **Never** hit submit. (It will cause the entire payroll to be paid the next day)
1. Click Save when done.
Note: Make sure to file all appropriate documentation in BambooHR. Also, if the employee has been employed for less than six months, check the notes section in BambooHR to see if they were referred by anyone at GitLab. If so, process a discretionary bonus for that team member as well.

## Visa Processing

To transfer an H1B Visa, GitLab needs to initiate the transfer before the team member can start work. In the past we have used Immigration Law Group as our legal counsel to help us through these steps.

1. People Ops should reach out to legal counsel to start the process.
1. Legal Counsel will reach out to the new team member and their manager to gather all information in regards to the transfer itself and the position.
1. If there is a need for a rush in processing the H1B (plans for travel outside the US, for example) then you can file the transfer under premium processing, if approved by the CEO.
1. Once the documents have been prepared by the law group, they will be sent to the CFO for wet signatures, and then returned to the law group.
1. Once the law group receives the signed forms, they can file the transfer with USCIS. Note: Until we receive confirmation that the transfer paperwork has been received by USCIS, the new team member cannot start work at GitLab.
1. Confirm to legal counsel when the new team member starts work at GitLab.
1. File all documentation in BambooHR surrounding the transfer.

## Offboarding

- **Remove a team member from Sundial**<a name="sundial-removal"></a>
1. Log in to Sundial using the link in the "Private Sundial URL" Google Doc  
1. Go to timezone view in the top right corner
1. Hover over the team member's name that you want to remove
1. Make sure you are in list view at this point, and then click the "x" to remove the team member

### Returning property to GitLab
{: #returning-property}

As part of [offboarding](https://about.gitlab.com/handbook/offboarding/), any GitLab property needs to be returned to GitLab. GitLab will pay for the shipping either by People Ops sending a FedEx shipping slip or it can be returned by another mutually agreed method. If property is not returned, GitLab reserves the right to use a creditor company to help retrieve the property.

#### FedEx info for the team member

Pull up the departing contributors home address. Find a Fedex location close to them. Then follow the template below.

"Hi [Name of team member]

The closest Fedex to you is located on [full street address, city, state, zip code]. Driving or walking directions can be found here: [enter link from “mapping home location to the Fedex location”]

Once there you can use Fedex boxes that you need and do the following actions:

1. Package the equipment in the box
2. Send package to GitLab HQ in San Francisco
3. Mark on the invoice "Bill Recipient"
4. Our Fedex number is: [find the number in Secretarial Vault in 1Password]
5. Provide People Ops the tracking number via email

Please let People Ops know if you have any questions."
